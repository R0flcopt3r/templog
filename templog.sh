#!/usr/bin/env bash

cpu_temp(){
	printf "%s" "$(sensors | grep "Core" | awk '
		BEGIN{
			count = 0
		}
		{
			clock[count++] = substr($3, 2, length($3)-3)
		}
		END {
			for (i=0; i<count; i++){
				printf ("%s, ", clock[i])
			}
		}'
	)"
}

cpu_clock(){
	printf "%s" "$(grep "MHz" /proc/cpuinfo | awk '
		BEGIN{
				count = 0
		}
		{
				clock[count++] = $4
		}
		END {
				for (i=0; i<count; i++){
						printf ("%s, ", clock[i])
				}
		}'
	)"
}

fan_speed(){
	printf "%s" "$(sensors | grep -E "fan[$1-$2]" | awk '
		BEGIN{
			count = 0
		}
		{
			clock[count++] = $2
		}
		END {
			for (i=0; i<count; i++){
				printf ("%s, ", clock[i])
			}
		}'
	)"
}



echo "time, liquid temp, motherboard, core 0 temp, core 1 temp, core 2 temp, core 3 temp, core 4 temp, core 5 temp, core 0 clock, core 1 clock, core 2 clock, core 3 clock, core 4 clock, core 5 clock, core 6 clock, core 7 clock, core 8 clock, core 9 clock, core 10 clock, core 11 clock, sys fan 1, sys fan 2, sys fan 3, cpu fan, gpu temp, gpu fan, gpu usage, gpu_clock
cpu usage,"
while true
do
	liquid=$(liquidctl status | grep Liquid | awk '{print $4}' | grep -E -o "[0-9]+\.[0-9]")
	cpu_fan=$(liquidctl status | grep "Fan speed" | awk '{print $4}')
	motherboard=$(sensors | grep "SYSTIN" | awk '{print $2}' | grep -E -o "[0-9]+\.[0-9]")
	gpu_temp=$(nvidia-smi -q -x --dtd | xpath -n -q -e "/nvidia_smi_log/gpu/temperature/gpu_temp/text()" | awk '{print $1}')
	gpu_fan=$(nvidia-smi -q -x --dtd | xpath -n -q -e "/nvidia_smi_log/gpu/fan_speed/text()" | awk '{print $1}')
	gpu_usage=$(nvidia-smi -q -x --dtd | xpath -n -q -e "/nvidia_smi_log/gpu/utilization/gpu_util/text()" | awk '{print $1}')
	gpu_clock=$(nvidia-smi -q -x --dtd | xpath -n -q -e "/nvidia_smi_log/gpu/clocks/graphics_clock/text()" |awk '{print $1}')

	echo " $SECONDS, $liquid, $motherboard, $(cpu_temp) $(cpu_clock) $(fan_speed 3 5) $cpu_fan, $gpu_temp, $gpu_fan, $gpu_usage, $gpu_clock"
	sleep 1
done

# other possible gpu stats
		# <graphics_clock>139 MHz</graphics_clock>
		# <sm_clock>139 MHz</sm_clock>
		# <mem_clock>405 MHz</mem_clock>
		# <video_clock>544 MHz</video_clock>
